'use strict'
//conexion a express
const express = require('express');

//path
var path= require('path');

//variable que contendra a express
var app = express();

//Secciones

//Cargar Rutas
var user_routes = require('./routes/user');
var follow_routes = require('./routes/follow');
var publication_routes = require('./routes/publication');
var message_routes = require('./routes/message');

//Cargar middelewares
app.use(express.urlencoded({extended: true}));
app.use(express.json());



//Cors cabeceras cada vez que hagamos una peticion pasa por este middelware y establece las cabeceras de forma correcta
// configurar cabeceras http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});

//Rutas etc
app.use('/', express.static('frontEnd', {redirect: false}));
app.use('/api', user_routes);
app.use('/api', follow_routes);
app.use('/api', publication_routes);
app.use('/api', message_routes);

/* reescribe la url de paginas virtuales para que angular pueda resfrescar su pagina */
app.get('*', function(req, res, next){
    res.sendFile(path.resolve('frontEnd/index.html'));
});


//exportar  
module.exports = app;