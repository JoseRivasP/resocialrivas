'use strict'

var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');
//libreria fileSystem de Node permite trabajar con archivos
var fs = require('fs');
//libreria path de node que permite trabajar con rutas del sistema de ficheros
var path = require('path');

//metodos de prueba
function home(req, res) {

    res.status(200).send({
        message: 'Home aplicacion'
    });

}

//funcion de prueba para ruta de prueba
function pruebas(req, res) {
    res.status(200).send({
        message: 'Accion de pruebas al Servidor WEB NodeJS'
    });

}

//registro de usuario
function saveUser(req, res) {
    var params = req.body;
    var user = new User();

    if (params.name && params.surname && params.nick && params.email && params.password) {
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;


        //controlar duplicados

        User.find({
            $or: [
                { nick: user.nick.toLowerCase() },
                { email: user.email.toLowerCase() }

            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error en la peticion de usuarios' });

            if (user && users.length >= 1) {
                return res.status(200).send({ message: 'El usuario que intentas registrar ya esta usado' });
            } else {
                //encriptar la password y guarda los datos
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;
                    user.save((err, userStore) => {
                        if (err) return res.status(500).send({ message: 'Error al guardar el usuario' });
                        if (userStore) {
                            res.status(200).send({ user: userStore });
                        } else {
                            res.status(404).send({ message: 'No se ha registrado un usuario' });
                        }
                    });
                });
            }
        });
    } else {
        res.status(200).send({
            message: 'Envia los campos necesarios!!'
        });
    }
}

//login
function loginUser(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({ email: email }, (err, user) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });
        if (user) {
            bcrypt.compare(password, user.password, (err, check) => {
                if (check) {

                    if (params.gettoken) {
                        //generar y devolver token
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        });
                    } else {
                        //Devolver datos de usuario
                        user.password = undefined;
                        return res.status(200).send({ user });
                    }
                } else {
                    return res.status(404).send({ message: 'El usuario no se ha podido identificar' });
                }
            });
        } else {
            return res.status(404).send({ message: 'El usuario no se ha podido identificar!!' });
        }
    });
}

//conseguir datos de un usuario
function getUser(req, res) {
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        if (!user) return res.status(404).send({ message: 'El usuario no existe' });

        followThisUser(req.user.sub, userId).then((value) => {
            user.password = undefined;
            return res.status(200).send({
                user,
                following: value.following,
                followed: value.followed
            });
        });

    });
}

async function followThisUser(identify_user_id, user_id) {
    var following = await Follow.findOne({ 'user': identify_user_id, 'followed': user_id }).exec().then((follow) => {
        return follow;
    }).catch((err) => {
        return handleError(err);
    });
    var followed = await Follow.findOne({ 'user': user_id, 'followed': identify_user_id }).exec().then((follow) => {
        return follow;
    }).catch((err) => {
        return handleError(err);
    });

    return {
        following: following,
        followed: followed
    }
}

//devolver un listado de usuarios
function getUsers(req, res) {
    var identify_user_id = req.user.sub;

    var page = 1;
    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        if (!users) return res.status(404).send({ message: 'No hay usuarios disponibles' });

        followUserIds(identify_user_id).then((value) => {
            return res.status(200).send({
                users,
                users_following: value.following,
                users_follow_me: value.followed,
                total,
                pages: Math.ceil(total / itemsPerPage)
            });
        });

    });
}

//Devuelve listados de usuarios que seguimos y que nos siguen funcion auxiliar asyncrona
async function followUserIds(user_id) {
    var following = await Follow.find({ 'user': user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec().then((follows) => {
        var follow_clean = [];

        follows.forEach((follow) => {
            follow_clean.push(follow.followed);
        });

        return follow_clean;

    }).catch((err) => {

        return handleError(err);

    });

    var followed = await Follow.find({ 'followed': user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec().then((follows) => {
        var follow_clean = [];

        follows.forEach((follow) => {
            follow_clean.push(follow.user);
        });

        return follow_clean;

    }).catch((err) => {

        return handleError(err);

    });

    return {
        following: following,
        followed: followed

    }

}

//metodo que genera un contador de seguidores tanto user como followed
function getCounters(req, res) {
    var userId = req.user.sub;
    if (req.params.id) {
        userId = req.params.id;
    }
    getCountFollow(userId).then((value) => {
        return res.status(200).send(value);
    });
}

//Funcion asycrona que tiene varios metodos sincronos encapsulados en variables
//funcion auxiliar de getCounters()
async function getCountFollow(user_id) {
    var following = await Follow.countDocuments({ 'user': user_id }).exec().then((count) => {
        return count;

    }).catch((err) => {

        return err;

    });

    var followed = await Follow.countDocuments({ 'followed': user_id }).exec().then((count) => {
        return count;

    }).catch((err) => {

        return err;

    });

    var publications = await Publication.countDocuments({ 'user': user_id }).exec().then((count) => {
        return count;

    }).catch((err) => {

        return err;

    });

    return {
        following: following,
        followed: followed,
        publications: publications
    }
}


//Edicion de datos de usuario
function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;

    //borrar propiedad password
    delete update.password;

    if (userId != req.user.sub) {
        return res.status(500).send({ message: 'No tienes permiso para actualizar los datos del usuario' });
    }

    User.find({
        $or: [
            { nick: update.nick.toLowerCase() },
            { email: update.email.toLowerCase() }
        ]

    }).exec((err, users) => {
        var userNotEqual = false;
        users.forEach((user) => {
            if (user && user._id != userId) return userNotEqual = true;
        });
        if (userNotEqual) return res.status(404).send({ message: 'El Nickname o el Correo ya estan en uso en nuestra plataforma' });
        User.findByIdAndUpdate(userId, update, { new: true }, (error, userUpdated) => {
            if (error) return res.status(200).send({ message: 'Error en la petición' });
            if (!userUpdated) return res.status(404).send({ message: 'No se ha podido Actualizar los datos del usuario' });
            return res.status(200).send({ user: userUpdated });
        });
    });


}

// subir archivos de imagen/avatar usuario

function uploadImage(req, res) {
    var userId = req.params.id;

    try {
        var file_path = req.files.image.path;

        var file_split = file_path.split('/');
        var file_name = file_split[2];
        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];


        if (userId != req.user.sub) {
            return removeFilesOfUploads(res, file_path, 'No tienes permiso para actualizar los datos del usuario');
        }

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            // Actualizar documento de usuario logueado
            User.findByIdAndUpdate(userId, { image: file_name }, { new: true }, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error en la petición' });
                if (!userUpdated) return res.status(404).send({ message: 'No se ha podido Actualizar los datos del usuario' });
                return res.status(200).send({ user: userUpdated });
            });
        } else {
            //en caso de que la extension sea mala
            return removeFilesOfUploads(res, file_path, 'La extesion no es Valida');
        }

    } catch {
        return res.status(200).send({ message: 'No se han subido Imagenes' });
    }
}

//funcion auxiliar de uploadImage()
function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(200).send({ message: message });
    });
}

//devuelve imagen usuario
function getImageFile(req, res) {
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/' + image_file;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen...' });
        }
    });

}

module.exports = {
    home, pruebas, saveUser, loginUser, getUser, getUsers, getCounters, updateUser, uploadImage, getImageFile
}