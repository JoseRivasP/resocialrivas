'use strict'

//var path = require('path');
//var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');
var User = require('../models/user');
var Follow = require('../models/follow');

//funcion de prueba para ruta de prueba
function prueba(req, res) {
    res.status(200).send({
        message: 'Hola Desde Follow'
    });

}
//guardar un seguimiento
function saveFollow(req, res) {
    var params = req.body;

    var follow = new Follow();
    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err, followStored) => {
        if (err) return res.status(500).send({ messaje: 'Error Al guardar el Seguimiento' });
        if (!followStored) return res.status(404).send({ message: 'El seguimiento no se ha guardado' });
        return res.status(200).send({ follow: followStored });
    })
}

//borrar un seguimiento
function deleteFollow(req, res) {
    var userId = req.user.sub;
    var followId = req.params.id;

    Follow.find({ 'user': userId, 'followed': followId }).deleteOne(err => {
        if (err) return res.status(500).send({ message: 'Error al dejar el seguimiento' });
        return res.status(200).send({ message: 'El follow se ha eliminado¡!' });
    });
}

//Lista de usuarios que sigo
function getFollowingUsers(req, res) {
    var userId = req.user.sub;

    if (req.params.id && req.params.page) {
        userId = req.params.id;
    }
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    } else {
        page = req.params.id;
    }

    var itemsPerPage = 4;

    Follow.find({ user: userId }).populate({ path: 'followed' }).paginate(page, itemsPerPage, (err, follows, total) => {
        if (err) return res.status(500).send({ message: 'Error en el Servidor' });
        if (!follows) return res.status(404).send({ message: 'No estas siguiendo a ningun usuario' });
        followUserIds(req.user.sub).then((value) => {
            return res.status(200).send({
                total: total,
                pages: Math.ceil(total / itemsPerPage),
                follows,
                users_following: value.following,
                users_follow_me: value.followed
            });
        });
    });

}

//listado de usuarios que nos siguen
function getFollowedUsers(req, res) {
    var userId = req.user.sub;

    if (req.params.id && req.params.page) {
        userId = req.params.id;
    }
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    } else {
        page = req.params.id;
    }

    var itemsPerPage = 4;

    Follow.find({ followed: userId }).populate('user').paginate(page, itemsPerPage, (err, follows, total) => {
        if (err) return res.status(500).send({ message: 'Error en el Servidor' });
        if (!follows) return res.status(404).send({ message: 'No te sigue ningun usuario' });
        followUserIds(req.user.sub).then((value) => {
            return res.status(200).send({
                total: total,
                pages: Math.ceil(total / itemsPerPage),
                follows,
                users_following: value.following,
                users_follow_me: value.followed
            });
        });
    });

}

//Devolver listado de usuarios que sigo y que me siguen 
//dependiendo de lo que se envie por url el user o el followed


function getMyFollows(req, res) {
    var userId = req.user.sub;

    var find = Follow.find({ user: userId });

    if (req.params.followed) {
        find = Follow.find({ followed: userId });
    }

    find.populate('user followed').exec((err, follows) => {
        if (err) return res.status(500).send({ message: 'Error en el Servidor' });
        if (!follows) return res.status(404).send({ message: 'No sigues a ningun usuario' });
        return res.status(200).send({ follows });
    });
}



//Devuelve listados de usuarios que seguimos y que nos siguen funcion auxiliar asyncrona
async function followUserIds(user_id) {
    var following = await Follow.find({ 'user': user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec().then((follows) => {
        var follow_clean = [];

        follows.forEach((follow) => {
            follow_clean.push(follow.followed);
        });

        return follow_clean;

    }).catch((err) => {

        return handleError(err);

    });

    var followed = await Follow.find({ 'followed': user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec().then((follows) => {
        var follow_clean = [];

        follows.forEach((follow) => {
            follow_clean.push(follow.user);
        });

        return follow_clean;

    }).catch((err) => {

        return handleError(err);

    });

    return {
        following: following,
        followed: followed

    }

}




module.exports = {
    prueba,
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows

}