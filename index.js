'use strict'

//variable que contiene mongoose
const mongoose = require('mongoose');

//variable que contiene express y body-parser
var app = require('./app');

//variables de socket.io
const http = require('http').createServer(app);
const io = require('socket.io')(http);

//variable que contiene el puerto a utilizar para la conexion
var port = 3800;

//constante con configuraciones de MongoDB
const configMongo = { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true };

//constante con url de nuestro Mongo DB y la base a conectarse
const urlMongo = 'mongodb://127.0.0.1:27017/redSocialMean';

//configuracion de Promise de MongoDB
mongoose.Promise = global.Promise;

//conexion socket.io
io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('my message', (msg) => {
        console.log('message: ' + msg);
        if(msg){
            io.emit("my message", msg);
        }
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
    
});

http.listen(3000, () => {
    console.log('Servidor Socket.io corriendo en http://localhost:3000');
});

//conexion a Base de Datos
mongoose.connect(urlMongo, configMongo).then(() => {
    console.log("La conexion a la base de datos redSocialMean se a realizado con exito");
    //Crear Servidor Web
    app.listen(port, () => {
        console.log("Servidor corriendo en http://localhost:3800");
    });
})
    .catch(err => console.log(err)); 