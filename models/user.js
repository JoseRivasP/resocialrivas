'use strict'

//variable que contine mongoose
 var mongoose = require('mongoose');

 //variable que contine el esquema del modelo
 var Schema = mongoose.Schema;

 // variable que contiene el esquema del modelo user
 var UserSchema = new Schema({
     name: String,
     surname: String,
     nick: String,
     email: String,
     password: String,
     role: String,
     image: String
 });

 // exportamos el modelo
 module.exports = mongoose.model('User', UserSchema);