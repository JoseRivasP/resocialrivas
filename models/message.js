'use strict'

//variable que contine mongoose
 var mongoose = require('mongoose');

 //variable que contine el esquema del modelo
 var Schema = mongoose.Schema;

 // variable que contiene el esquema del modelo publication
 var MessageSchema = new Schema({
     text: String,
     viewed: String,
     created_at: String,
     emitter: {type: Schema.ObjectId, ref: 'User'},
     receiver: {type: Schema.ObjectId, ref: 'User'}
     
 });

 // exportamos el modelo
 module.exports = mongoose.model('Message', MessageSchema);