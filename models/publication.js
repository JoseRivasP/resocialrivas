'use strict'

//variable que contine mongoose
 var mongoose = require('mongoose');

 //variable que contine el esquema del modelo
 var Schema = mongoose.Schema;

 // variable que contiene el esquema del modelo publication
 var PublicationSchema = new Schema({
     texto: String,
     file: String,
     created_at: String,
     user: {type: Schema.ObjectId, ref: 'User'}
 });

 // exportamos el modelo
 module.exports = mongoose.model('Publication', PublicationSchema);